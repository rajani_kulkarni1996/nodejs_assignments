var async = require('async');
var variable1 = 6, variable2 = 2;
async.parallel([
function(callback) {
    sum = variable1 + variable2;
    console.log(sum);
    callback();
    
},

function (callback) {
    difference = variable1 - variable2;
    console.log(difference);
    callback();
},

function (callback) {
    product = variable1 * variable2;
    console.log(product);
    callback();
},

function (callback) {
    division = variable1 / variable2;
    console.log(division);
    callback();
},

function(callback){
console.log("All independent operations are executed");
}
])
